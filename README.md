Pipleine CI/CD

# Cabeçalho nível 1 (maior)
## Cabeçalho nível 2
###### Cabeçalho nível 6 (menor)


Negrito: **Texto em negrito**, __Texto em negrito__

Itálico: *Texto em itálico*, _Texto em itálico_

Tachado: ~~Texto tachado~~

Negrito e itálico: **_Texto negrito e itálico_**, _**Texto negrito e itálico**_

Como diria o meu avô:
> Água mole, pedra dura, tanto bate até que fura.


`` `
Exemplo de **código**
que pode ser usado em *blocos*
`` `

[Link para cabeçalho](#nome-do-cabecalho)
[Link para linha](caminho/arquivo#L13)
[Link relativo](caminho/arquivo.md)
[Link relativo](./caminho/arquivo.md)
[Link absoluto](/caminho/arquivo.md)
[Link com texto](https://www.dominio.com)

- Primeiro
- Segundo
    - Primeiro filho
    - Segundo filho
        - Primeiro neto
1. Primeiro
    - Filho
1. Segundo
1. Terceiro


"MD029": {
    "style":"ordered"
}


- [x] Tarefa 1
- [ ] Tarefa 2
- [ ] Tarefa 3
- [ ] \(Se iniciar com parênteses tem que escapar)

Basta usar a barra \*invertida\* para escapar


<nobr>Não serei quebrado</nobr>


| Primeiro cabeçalho | Segundo cabeçalho |
| ------------------ | ----------------- |
| Conteúdo célula    | Conteúdo célula   |
| Conteúdo célula    | Conteúdo célula   |


| Primeiro cabeçalho  | Segundo cabeçalho |
| --- | --- |
| **célula negrito**  | \| Usando pipe    |
| *célula itálico*    | Conteúdo célula   |


![Amostra Vídeo](caminho/video.mp4)


![alt text](caminho/img.png)


`` `math
a^2+b^2=c^2
`` `


<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>


***
---
___


Este texto possui uma anotação de rodapé.[^1]
[^1]: Este é meu rodapé.



![Homem Letra] (https://raw.githubusercontent.com/professorjosedeassis/Linguagem-C/master/imagens/homem%20letra.gif)