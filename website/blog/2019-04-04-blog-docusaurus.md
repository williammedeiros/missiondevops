---
title: Docusaurus.io
author: William Medeiros
authorURL: http://twitter.com/
authorFBID: 100002976521003
---
##Vamos em frente


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

<!--truncate-->


```
-rwxr-xr-x    1 williammedeiros  staff    4065 26 Out  1985 README.md
-rw-r--r--    1 williammedeiros  staff  350557  2 Abr 13:24 package-lock.json
-rw-r--r--    1 williammedeiros  staff     375  2 Abr 13:24 package.json
drwxr-xr-x    3 williammedeiros  staff      96  2 Abr 13:24 core
drwxr-xr-x    3 williammedeiros  staff      96  2 Abr 13:24 pages
drwxr-xr-x    4 williammedeiros  staff     128  2 Abr 13:24 static
drwxr-xr-x  655 williammedeiros  staff   20960  2 Abr 13:30 node_modules
drwxr-xr-x    3 williammedeiros  staff      96  2 Abr 13:30 i18n
-rwxr-xr-x    1 williammedeiros  staff    3097  3 Abr 23:33 siteConfig.js
-rwxr-xr-x    1 williammedeiros  staff     192  4 Abr 00:03 sidebars.json
drwxr-xr-x    8 williammedeiros  staff     256  4 Abr 00:10 blog

```